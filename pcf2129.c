#include <errno.h>
#include <fcntl.h>
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define BIT(nr)             (1UL << (nr))
#define BIT_IS_SET(nr, x)   (!!((x) & BIT(nr)))

#define I2C_BUS_NUM     1
#define I2C_ADDR        0x51

/* Control registers */
#define PCF2129_REG_CTRL1               0x00
#define PCF2129_REG_CTRL2               0x01
#define PCF2129_REG_CTRL3               0x02
/* Time and date registers */
#define PCF2129_REG_SECONDS             0x03
#define PCF2129_REG_MINUTES             0x04
#define PCF2129_REG_HOURS               0x05
#define PCF2129_REG_DAYS                0x06
#define PCF2129_REG_WEEKDAYS            0x07
#define PCF2129_REG_MONTHS              0x08
#define PCF2129_REG_YEARS               0x09
/* Alarm registers */
#define PCF2129_REG_SECOND_ALARM        0x0a
#define PCF2129_REG_MINUTE_ALARM        0x0b
#define PCF2129_REG_HOUR_ALARM          0x0c
#define PCF2129_REG_DAY_ALARM           0x0d
#define PCF2129_REG_WEEKDAY_ALARM       0x0e
/* CLKOUT control register */
#define PCF2129_REG_CLKOUT_CTRL         0x0f
/* Watchdog registers */
#define PCF2129_REG_WATCHDOG_TIM_CTL    0x10
#define PCF2129_BIT_WD_CTL_TF0          BIT(0)
#define PCF2129_BIT_WD_CTL_TF1          BIT(1)
#define PCF2129_BIT_WD_CTL_TI_TP        BIT(5)
#define PCF2129_BIT_WD_CTL_WD_CD        BIT(7)
#define PCF2129_REG_WATCHDOG_TIM_VAL    0x11


#ifdef SHADOW_REG
struct shadow_reg {
    uint8_t val;
    struct {
        uint8_t valid : 1;
        uint8_t allow : 1;
    };
};
#endif

typedef struct pcf2129 {
    unsigned int max_register;
#ifdef SHADOW_REG
    struct shadow_reg *shadow;
#endif
    int fd;
    uint8_t addr;
    uint8_t bus_num;
} pcf2129_t;

void print_reg_func(const char *name, uint8_t val, unsigned int bitpos, unsigned int bits)
{
    if (bits == 1) {
        val = BIT_IS_SET(bitpos, val);
        printf("\t%s:%d\n", name, val);
    } else {
        val = (val >> bitpos) & (0xff >> (8 - bits));
        printf("\t%s:0x%x\n", name, val);
    }
}

void print_control1(uint8_t val)
{
    print_reg_func("EXT_TEST", val, 7, 1);
    print_reg_func("STOP", val, 5, 1);
    print_reg_func("TSF1", val, 4, 1);
    print_reg_func("POR_OVRD", val, 3, 1);
    print_reg_func("12_24", val, 2, 1);
    print_reg_func("MI", val, 1, 1);
    print_reg_func("SI", val, 0, 1);
}

void print_control2(uint8_t val)
{
    print_reg_func("MSF", val, 7, 1);
    print_reg_func("WDTF", val, 6, 1);
    print_reg_func("TSF2", val, 5, 1);
    print_reg_func("AF", val, 4, 1);
    print_reg_func("TSIE", val, 2, 1);
}

void print_control3(uint8_t val)
{
    print_reg_func("PWRMNG[2:0]", val, 5, 3);
    print_reg_func("BTSE", val, 4, 1);
    print_reg_func("BF", val, 3, 1);
    print_reg_func("BLF", val, 2, 1);
    print_reg_func("BIE", val, 1, 1);
    print_reg_func("BLIE", val, 0, 1);
}

void print_seconds(uint8_t val)
{
    print_reg_func("OSF", val, 7, 1);
    print_reg_func("SECONDS", val, 0, 7);
}

void print_minutes(uint8_t val)
{
    print_reg_func("MINUTES", val, 0, 7);
}

void print_clkout(uint8_t val)
{
    print_reg_func("TCR[1:0]", val, 6, 2);
    print_reg_func("OTPR", val, 5, 1);
    print_reg_func("COF[2:0]", val, 0, 3);
}

void print_watchdg_tim_ctl(uint8_t val)
{
    print_reg_func("WD_CD", val, 7, 1);
    print_reg_func("TI_TP", val, 5, 1);
    print_reg_func("TF", val, 0, 2);
}

void print_watchdg_tim_val(uint8_t val)
{
    print_reg_func("WATCHDOG_TIM_VAL", val, 0, 8);
}

void print_timestamp_ctrl(uint8_t val)
{
    print_reg_func("TSM", val, 7, 1);
    print_reg_func("TSOFF", val, 6, 1);
    print_reg_func("1_O_16_TIMESTP[4:0]", val, 0, 5);
}

static int pcf2129_init(pcf2129_t *pcf2129, unsigned int bus_num, uint8_t addr)
{
    char filename[20];

    pcf2129->bus_num = bus_num;
    pcf2129->addr = addr;
    snprintf(filename, 19, "/dev/i2c-%d", bus_num);
    pcf2129->fd = open(filename, O_RDWR);
    if (pcf2129->fd < 0) {
        fprintf(stderr, "ERROR: Can't open i2c-%d: %s", bus_num,
                strerror(errno));
        return -1;
    }

    pcf2129->max_register = 0x1d;
#ifdef SHADOW_REG
    pcf2129->shadow = calloc(pcf2129->max_register, sizeof(struct shadow_reg));
    if (!pcf2129->shadow) {
        fprintf(stderr, "ERROR: Can't allocate %zubytes of memory: %s\n",
                sizeof(struct shadow_reg) * pcf2129->max_register,
                strerror(errno));
        close(pcf2129->fd);
        return -1;
    }
#endif

//    pcf2129->shadow[PCF2129_REG_CTRL1].allow = 1;

    if (ioctl(pcf2129->fd, I2C_SLAVE_FORCE, pcf2129->addr) < 0) {
        fprintf(stderr, "ERROR: Can't access device 0x%02x: %s\n", pcf2129->addr,
                strerror(errno));
        close(pcf2129->fd);
    }

    return 0;
}

static int pcf2129_read(pcf2129_t *pcf2129, uint8_t reg, uint8_t *val)
{
    int ret;

#ifdef SHADOW_REG
    if (pcf2129->shadow[reg].allow && pcf2129->shadow[reg].valid) {
        *val = pcf2129->shadow[reg].val;
        return 0;
    }
#endif
//    fprintf(stderr, "DEBUG: write 0x%02x\n", reg);
    ret = write(pcf2129->fd, &reg, 1);
    if (ret != 1) {
        fprintf(stderr, "ERROR: Can't write register 0x%02x: %s\n",
                reg, strerror(errno));
        return -1;
    }

    ret = read(pcf2129->fd, val, 1);
    if (ret != 1) {
        fprintf(stderr, "ERROR: Can't read register 0x%02x: %s\n",
                reg, strerror(errno));
        return -1;
    }
//    fprintf(stderr, "DEBUG: got 0x%02x\n", *val);
#ifdef SHADOW_REG
    if (pcf2129->shadow[reg].allow)
        pcf2129->shadow[reg].val = *val;
#endif
    return 0;
}

int pcf2129_print_reg(pcf2129_t *pcf2129, uint8_t reg)
{
    int ret;
    uint8_t val;
    ret = pcf2129_read(pcf2129, reg, &val);
    if (ret < 0)
        return ret;

    switch (reg)
    {
    case 0x00:
        printf("00h Control_1:\t0x%02x\n", val);
        print_control1(val);
        break;
    case 0x01:
        printf("01h Control_2:\t0x%02x\n", val);
        print_control2(val);
        break;
    case 0x02:
        printf("02h Control_3:\t0x%02x\n", val);
        print_control3(val);
        break;
    case 0x03:
        printf("03h Seconds:\t0x%02x\n", val);
        print_seconds(val);
        break;
    case 0x04:
        printf("04h Minutes:\t0x%02x\n", val);
        print_minutes(val);
        break;
    case 0x0f:
        printf("0fh CLKOUT_ctl:\t0x%02x\n", val);
        print_clkout(val);
        break;
    case 0x10:
        printf("10h Watchdg_tim_ctl:\t0x%02x\n", val);
        print_watchdg_tim_ctl(val);
        break;
    case 0x11:
        printf("11h Watchdg_tim_val:\t0x%02x\n", val);
        print_watchdg_tim_val(val);
        break;
    case 0x12:
        printf("12h Timestp_ctl:\t0x%02x\n", val);
        print_timestamp_ctrl(val);
        break;
    default:
        printf("0x%02x: 0x%02x\n", reg, val);
        break;
    }

    return 0;
}

int pcf2129_write(pcf2129_t *pcf2129, uint8_t reg, uint8_t val)
{
    int ret;
    uint8_t buf[2];
#ifdef SHADOW_REG
    if (pcf2129->shadow[reg].allow)
        pcf2129->shadow[reg].valid = 0;
#endif
//    fprintf(stderr, "DEBUG: write reg:0x%02x val:0x%02x\n", reg, val);

    buf[0] = reg;
    buf[1] = val;
    ret = write(pcf2129->fd, &buf, 2);
    if (ret != 2) {
        fprintf(stderr,
                "ERROR: Can't write 0x%02x to rgister 0x%02x: %s\n",
                val, reg, strerror(errno));
        return -1;
    }

    return 0;
}

int main(int argc, char * argv[])
{
    int ret;
    pcf2129_t pcf2129;
    uint8_t reg = 0xff;
    uint8_t val;

    ret = pcf2129_init(&pcf2129, I2C_BUS_NUM, I2C_ADDR);
    if (ret < 0) {
        exit(EXIT_FAILURE);
    }

    if (argc > 3) {
        fprintf(stderr, "ERROR: Invalid number of arguments.\n");
        exit(EXIT_FAILURE);
    }
    if (argc > 1) {
        char *eptr;
        unsigned long input;

        input = strtoul(argv[1], &eptr, 0);
        if (input > 0x1b) {
            errno = ERANGE;
            fprintf(stderr, "ERROR: Invalid register: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } else if (*eptr != '\0') {
            fprintf(stderr, "ERROR: Invalid register: '%s'\n", argv[1]);
            exit(EXIT_FAILURE);
        }
        reg = (uint8_t)input;
    }
    if (argc > 2) {
        char *eptr;
        unsigned long input;

        input = strtoul(argv[2], &eptr, 0);
        if (input > 0xff) {
            errno = ERANGE;
            fprintf(stderr, "ERROR: Invalid value: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        } else if (*eptr != '\0') {
            fprintf(stderr, "ERROR: Invalid value: '%s'\n", argv[2]);
            exit(EXIT_FAILURE);
        }
        val = (uint8_t)input;
    }

    /* set watchdog timer to 1Hz */
//    val = PCF2129_BIT_WD_CTL_TF1;
    /* enable watchdog timer */
//    val |= PCF2129_BIT_WD_CTL_WD_CD;
//    pcf2129_write(&pcf2129, PCF2129_REG_WATCHDOG_TIM_CTL, val);

    if (argc == 1)
        for (uint8_t reg = 0; reg < 0x1a; reg++)
            pcf2129_print_reg(&pcf2129, reg);
    else if (argc == 2)
        ret = pcf2129_print_reg(&pcf2129, reg);
    else if (argc == 3) {
        pcf2129_print_reg(&pcf2129, reg);
        ret = pcf2129_write(&pcf2129, reg, val);
        pcf2129_print_reg(&pcf2129, reg);
    }
    return ret;
}